package MineSweeper.model;

public class CoordinateValidator {

    private int wide;
    private int height;

    public CoordinateValidator(int wide, int height) {
        this.wide = wide;
        this.height = height;
    }

    public boolean validate(Coordinate coordinate) {
        return (coordinate.getX() >= 0 && coordinate.getX() < wide &&
                coordinate.getY() >= 0 && coordinate.getY() < height);
    }
}
