package MineSweeper.model;

public enum GameState {

    PLAYING,
    LOST,
    WON

}
