package MineSweeper.model;

public enum CellState {
    ACTIVE,
    INACTIVE,
    FLAGGED
}
