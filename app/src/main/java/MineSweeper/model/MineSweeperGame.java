package MineSweeper.model;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class MineSweeperGame {

    private static final int EASY_WIDE_VALUE = 10;
    private static final int EASY_HEIGHT_VALUE = 8;
    private static final int EASY_MINE_QUANTITY = 10;

    private final int boardHeight;
    private final int mineQuantity;
    private final int boardWide;
    private GameState gameState;
    private GameCell[][] gameBoard;
    private Set<Coordinate> mineCoordinatesList;
    CoordinateValidator validator;

    public MineSweeperGame() {
        this(EASY_WIDE_VALUE, EASY_HEIGHT_VALUE, EASY_MINE_QUANTITY);
    }

    private MineSweeperGame(int wide, int height, int mineQuantity){
        this.boardWide = wide;
        this.boardHeight = height;
        this.mineQuantity = mineQuantity;
        this.gameState = GameState.PLAYING;
        validator = new CoordinateValidator(boardWide, boardHeight);
        generateGameBoard();
    }

    private void fillGameBoardWithZeros() {
        for (int y = 0; y < boardHeight; y++) {
            for (int x = 0; x < boardWide; x++) {
                gameBoard[y][x] = new GameCell();
            }
        }
    }

    private void generateGameBoard() {
        gameBoard = new GameCell[boardHeight][boardWide];
        fillGameBoardWithZeros();
        generateMines(boardWide, boardHeight);
    }

    private void generateMines(int maxXValue, int maxYValue) {
        mineCoordinatesList = new HashSet<>();
        Random numberGenerator = new Random();
        while (mineCoordinatesList.size() < mineQuantity) {
            int randomXValue = numberGenerator.nextInt(maxXValue);
            int randomYValue = numberGenerator.nextInt(maxYValue);
            mineCoordinatesList.add(new Coordinate(randomXValue, randomYValue));
        }
        setMinesInGameBoard();
    }

    private void setMinesInGameBoard() {
        for (Coordinate mineCoordinate : mineCoordinatesList) {
            int x = mineCoordinate.getX();
            int y = mineCoordinate.getY();
            gameBoard[y][x].turnIntoMine();
            addOneToCell(x - 1, y - 1);
            addOneToCell(x + 1, y - 1);
            addOneToCell(x - 1, y + 1);
            addOneToCell(x + 1, y + 1);
            addOneToCell(x, y - 1);
            addOneToCell(x - 1, y);
            addOneToCell(x + 1, y);
            addOneToCell(x, y + 1);
        }
    }

    private void addOneToCell(int x, int y) {
        if (validator.validate(new Coordinate(x, y))){
            gameBoard[y][x].addOne();
        }
    }

    private void revealAdjacentCellsIfZero(int x, int y) {
        if (gameBoard[y][x].getCurrentCellContent() == CellContent.ZERO){
            revealWithoutRisk(x - 1, y - 1);
            revealWithoutRisk(x + 1, y - 1);
            revealWithoutRisk(x - 1, y + 1);
            revealWithoutRisk(x + 1, y + 1);
            revealWithoutRisk(x, y - 1);
            revealWithoutRisk(x - 1, y);
            revealWithoutRisk(x + 1, y);
            revealWithoutRisk(x, y + 1);
        }
    }

    private void revealWithoutRisk(int x, int y) {
        if (validator.validate(new Coordinate(x, y))
                && gameBoard[y][x].getCurrentCellContent() != CellContent.MINE
                && gameBoard[y][x].getCurrentCellState() != CellState.ACTIVE){
            gameBoard[y][x].activateCellForce();
            revealAdjacentCellsIfZero(x, y);
        }
    }

    private void checkWin() {
        for (GameCell[] row : gameBoard) {
            for (GameCell cell : row) {
                if (cell.getCurrentCellContent() != CellContent.MINE && cell.getCurrentCellState() != CellState.ACTIVE) {
                    return;
                }
            }
        }
        gameState = GameState.WON;
    }

    private void activateAllMines() {
        for (Coordinate mineCoordinate : mineCoordinatesList) {
            int x = mineCoordinate.getX();
            int y = mineCoordinate.getY();
            gameBoard[y][x].activateCell();
        }
    }

    private boolean isAPlayableMove(int x, int y) {
        return validator.validate(new Coordinate(x, y)) && gameBoard[y][x].getCurrentCellState() == CellState.INACTIVE;
    }

    public void resetGameBoard() {
        gameState = GameState.PLAYING;
        fillGameBoardWithZeros();
        generateMines(boardWide, boardHeight);
    }

    public boolean playCell(Coordinate playCoordinate) {
        int x = playCoordinate.getX();
        int y = playCoordinate.getY();
        if (gameState != GameState.PLAYING || !isAPlayableMove(x, y)) {
            return false;
        }
        gameBoard[y][x].activateCell();
        if (gameBoard[y][x].getCurrentCellContent() == CellContent.MINE) {
            gameState = GameState.LOST;
            activateAllMines();
            return true;
        }
        revealAdjacentCellsIfZero(x, y);
        checkWin();
        return true;
    }

    public boolean flagCell (Coordinate flagCoordinate) {
        int x = flagCoordinate.getX();
        int y = flagCoordinate.getY();
        if (validator.validate(flagCoordinate) && gameBoard[y][x].getCurrentCellState() != CellState.ACTIVE) {
            gameBoard[y][x].flagCell();
            return true;
        }
        return false;
    }

    public GameCell[][] getGameBoard() {
        return gameBoard;
    }

    public GameState getGameState() {
        return gameState;
    }
}
