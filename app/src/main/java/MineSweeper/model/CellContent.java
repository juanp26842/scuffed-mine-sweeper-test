package MineSweeper.model;

public enum CellContent {
    ZERO(0),
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    MINE(117);

    public final int numericValue;

    CellContent(int numericValue) {
        this.numericValue = numericValue;
    }
}
