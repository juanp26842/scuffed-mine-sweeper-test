package MineSweeper.model;

public class GameCell {

    private CellContent currentCellContent;
    private CellState currentCellState;

    public GameCell() {
        this(CellContent.ZERO, CellState.INACTIVE);
    }

    public GameCell(CellContent currentCellContent, CellState currentCellState) {
        this.currentCellContent = currentCellContent;
        this.currentCellState = currentCellState;
    }

    public CellContent getCurrentCellContent() {
        return currentCellContent;
    }

    public void setCurrentCellContent(CellContent currentCellContent) {
        this.currentCellContent = currentCellContent;
    }

    public CellState getCurrentCellState() {
        return currentCellState;
    }

    public void activateCell() {
        if (currentCellState != CellState.FLAGGED) {
            currentCellState = CellState.ACTIVE;
        }
    }

    public void activateCellForce() {
        if (currentCellState != CellState.FLAGGED) {
            currentCellState = CellState.ACTIVE;
        }
    }

    public void flagCell() {
        if (currentCellState == CellState.FLAGGED) {
            currentCellState = CellState.INACTIVE;
        } else if (currentCellState == CellState.INACTIVE){
            currentCellState = CellState.FLAGGED;
        }
    }

    public boolean addOne() {
        if (currentCellContent == CellContent.MINE) {
            return false;
        }
        int currentCellValue = currentCellContent.numericValue;
        currentCellContent = CellContent.values()[currentCellValue + 1];
        return true;
    }

    public void turnIntoMine() {
        currentCellContent = CellContent.MINE;
    }

}
