package MineSweeper.project;

import MineSweeper.model.CellContent;
import MineSweeper.model.CellState;
import MineSweeper.model.Coordinate;
import MineSweeper.model.GameCell;
import MineSweeper.model.GameState;
import MineSweeper.model.MineSweeperGame;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        MineSweeperGame game = new MineSweeperGame();
        printBoard(game);
        Scanner sc = new Scanner(System.in);
        while (game.getGameState() == GameState.PLAYING) {
            System.out.println("\nInput X coordinate");
            int x = sc.nextInt();
            System.out.println("\nInput Y coordinate");
            int y = sc.nextInt();
            if (game.playCell(new Coordinate(x, y))){
                System.out.println(String.format("\nAfter playing %d, %d\n", x, y));
            } else {
                System.out.println(String.format("\n[%d, %d] is an invalid move\n", x, y));
            }
            printBoard(game);
        }
        System.out.println(String.format("\nYou have %s\n", game.getGameState()));
        /* //TODO timer test IGNORE
        long startingTime = System.currentTimeMillis();
        long secondsPassed = 0;
        while (true) {
            long currentTimePassed = (System.currentTimeMillis() - startingTime) / 10;
            if (secondsPassed != currentTimePassed){
                StringBuilder timer = new StringBuilder();
                timer.append(currentTimePassed / 100);
                timer.append(".");
                timer.append(currentTimePassed % 100);
                System.out.println(timer);
                System.out.println(String.format("%d.%d", currentTimePassed / 100, currentTimePassed % 100));
                secondsPassed = currentTimePassed;
            }
            if (secondsPassed >= 500) {
                break;
            }
        }*/
    }

    private static void printBoard(MineSweeperGame game) {
        for (GameCell[] gameRow : game.getGameBoard()) {
            for (GameCell cell : gameRow) {
                if (cell.getCurrentCellState() == CellState.ACTIVE) {
                    System.out.print(cell.getCurrentCellContent() != CellContent.MINE? cell.getCurrentCellContent().numericValue + ", " : "M, ");
                } else {
                    System.out.print(cell.getCurrentCellState() == CellState.FLAGGED? "F, " : "B, ");
                }
            }
            System.out.println();
        }
    }
}
